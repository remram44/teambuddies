# Team Buddies

Small remake of the PS1 title Team Buddies (Psygnosis, 2000), mainly for me to try out the [Godot game engine](https://godotengine.org/). Soon to be open-sourced.

Development of the game is going nicely. I am not an artist though, so all the art is "placeholder" (my best attempts, based on the original PS1 game). Help welcome!

# See also

* [On itch.io](https://remram44.itch.io/team-buddies)
* [Roadmap](https://trello.com/b/2ArI5tPO/team-buddies)
