extends Spatial

const SPEED = 1
const DAMAGE = 5
var lifetime = 0.25

func _on_body_entered(body):
	if body.has_method("damage"):
		body.damage(global_transform.origin, "light", DAMAGE)
	queue_free()

func _physics_process(delta):
	lifetime -= delta
	if lifetime < 0:
		queue_free()
	translate(Vector3(0, 0, -SPEED))
