extends Spatial

const FIRING_DELAY = 0.15
const RECOIL = 12

onready var bullet_class = preload("res://weapons/LightBullet.tscn")
var world

var firing = false
var next_shot = 0
var character = null

func set_character(newchar):
	character = newchar

func _ready():
	world = self
	while world.get_name() != "World":
		world = world.get_parent()

func _physics_process(delta):
	next_shot -= delta
	while next_shot <= 0:
		if not firing:
			set_physics_process(false)
			next_shot = 0
			return
		next_shot += FIRING_DELAY

		if character.ammo >= 1:
			character.ammo -= 1

			var bullet = bullet_class.instance()
			bullet.global_transform = global_transform.translated(Vector3(0, 0, -1))
			world.add_child(bullet)
			character.recoil(global_transform.basis.xform(Vector3(0, 0, RECOIL)))

func trigger(pressed):
	firing = pressed
	if firing:
		set_physics_process(true)

func get_weapon_type():
	return "light"
