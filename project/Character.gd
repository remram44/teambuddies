extends KinematicBody

onready var utils = $"/root/utils"

const MOVE_SPEED = 12
const WALK_ACCEL = 2
const AIR_ACCEL = 1
const JUMP_SPEED = 10
const ROTATE_SPEED = 0.3
const GRAVITY = 0.4
const MAX_FALL_SPEED = 15

const WEAPON_ATTACH = "weapon"

export(String) var weapon_class = "" setget set_weapon, get_weapon
var weapon = null
var weapon_type = ""
var weapon_attach = null

onready var cam = $CamBase
onready var buddy = $Buddy
onready var anim = $Buddy/AnimationPlayer

var target = null
var locked = false

var controller = null

var health = 100
var ammo = 200

var velocity = Vector3(0, 0, 0)

func get_weapon():
	return weapon_class

func set_weapon(cls_name):
	var cls = load(cls_name)
	if not cls:
		weapon_class = null
		return
	weapon_class = cls_name

func change_weapon(cls_name, drop=false):
	# Remove old weapon
	if weapon != null:
		buddy.remove_child(weapon)
		weapon = null
		if drop:
			pass  # TODO: drop old weapon

	if not cls_name:
		return

	# Load class
	var cls = load(cls_name)
	if not cls:
		weapon_class = null
		return

	# Find attach point
	var skeleton = buddy.find_node("Skeleton")
	if skeleton:
		if skeleton.find_bone(WEAPON_ATTACH) != -1:
			weapon_attach = BoneAttachment.new()
			weapon_attach.set_bone_name(WEAPON_ATTACH)
			skeleton.add_child(weapon_attach)
		else:
			printerr("character %s has no bone named '%s'" % [self, WEAPON_ATTACH])
			weapon_attach = buddy
	else:
		printerr("character %s has no skeleton to attach weapon" % self)
		weapon_attach = buddy

	weapon_class = cls_name
	weapon = cls.instance()
	weapon.set_name("weapon")
	if weapon.has_method("get_weapon_type"):
		weapon_type = weapon.get_weapon_type()
	weapon_attach.add_child(weapon)
	weapon.set_character(self)

func set_controller(newctrl):
	controller = newctrl

func _ready():
	change_weapon(weapon_class)
	add_to_group("characters")
	add_to_group("targets")

func trigger(pressed):
	if weapon != null and weapon.has_method("trigger"):
		weapon.trigger(pressed)

func _process(delta):
	if target and not target.get_ref():
		target = null

	# Point weapon at target
	if weapon:
		var aim
		if target:
			aim = target.get_ref().translation
		else:
			aim = weapon.global_transform.origin
			aim += buddy.global_transform.basis.xform(Vector3(0, 0, -1))
		weapon.global_transform = weapon.global_transform.looking_at(aim, Vector3(0, 1, 0))

func turn_towards(direction):
	var target_angle = atan2(-direction.x, -direction.z)
	var chg = utils.angle_wrap(target_angle - buddy.rotation.y)
	buddy.rotation.y += utils.angle_wrap(clamp(chg, -ROTATE_SPEED, ROTATE_SPEED))

func _physics_process(delta):
	if target and not target.get_ref():
		target = null

	var move_vec
	var jump = false
	if controller:
		move_vec = controller.get_movement().normalized()
		if move_vec.y > 0:
			jump = true
		move_vec.y = 0
	else:
		move_vec = Vector3(0, 0, 0)

	# Turn
	if target and locked:
		turn_towards(target.get_ref().translation - translation)
	elif move_vec.length_squared() > 0.1:
		turn_towards(move_vec)

	# Walk
	var grounded = is_on_floor()
	var vel_change = move_vec * MOVE_SPEED - velocity
	vel_change.y = 0
	if vel_change:
		if grounded:
			vel_change *= min(1, WALK_ACCEL / vel_change.length())
		else:
			# Air control
			vel_change *= min(1, AIR_ACCEL / vel_change.length())
	velocity += vel_change

	# Fall or stay grounded
	velocity.y -= GRAVITY
	if grounded and velocity.y <= 0:
		velocity.y = -0.1
	if velocity.y < -MAX_FALL_SPEED:
		velocity.y = -MAX_FALL_SPEED

	# Jump
	var just_jumped = false
	if grounded and jump:
		just_jumped = true
		velocity.y = JUMP_SPEED

	# Move
	move_and_slide(velocity, Vector3(0, 1, 0))

	# Animation
	if just_jumped:
		play_anim("jump")
	elif grounded:
		if move_vec.x == 0 and move_vec.z == 0:
			play_anim("idle-loop")
		else:
			play_anim("walk-loop")

func play_anim(name):
	if not anim:
		return
	if weapon_type:
		var anim_weapon = "%s-%s" % [weapon_type, name]
		if anim.has_animation(anim_weapon):
			name = anim_weapon
	if anim.current_animation == name:
		return
	anim.play(name)

func recoil(vec):
	velocity += vec

func damage(location, type, amount):
	health -= amount
	print("HIT! h=%f" % health)
	if health <= 0:
		self.queue_free()
