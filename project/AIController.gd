extends Node


const MIN_DIST = 5 * 0.7
const MAX_DIST = 5 * 1.4

onready var character = $".."
var target
var evade_direction = 1

func _ready():
	# Attach to character
	character.set_controller(self)

func find_closest_character():
	var characters = get_tree().get_nodes_in_group("characters")
	var best = null
	var best_sqdist = 1000
	for ch in characters:
		if ch == character:
			continue
		var sqdist = (ch.translation - character.translation).length_squared()
		if sqdist < best_sqdist:
			best = ch
			best_sqdist = sqdist
	return best

func _physics_process(delta):
	target = find_closest_character()
	if not target:
		return
	if randf() < 0.06:
		evade_direction *= -1

func get_movement():
	if not target:
		return Vector3(0, 0, 0)
	# Approach player
	var move_vec = target.translation - character.translation
	if move_vec.length_squared() < MIN_DIST * MIN_DIST:
		# Too close, evade
		move_vec *= -1
	elif move_vec.length_squared() < MAX_DIST * MAX_DIST:
		# In range, circle around it
		move_vec = move_vec.cross(Vector3(0, evade_direction, 0))
	move_vec.y = 0
	return move_vec
