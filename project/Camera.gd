extends Camera

var character = null
onready var health_label = $Health
onready var ammo_label = $Ammo

func _ready():
	character = get_node("../..")

func _process(delta):
	health_label.text = "Health: %s" % character.health
	ammo_label.text = "Ammo: %s" % character.ammo
