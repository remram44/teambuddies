tool
extends MeshInstance

# Adapted from https://gist.github.com/TheHX/94a83dea1a0f932d5805
# Original copyright (c) 2015 Franklin Sobrinho.

export(Image) var heightmap setget set_heightmap, get_heightmap
export(String) var tilemap = "" setget set_tilemap, get_tilemap
export(int, 1, 64) var tileset_size = 1 setget set_tileset_size, get_tileset_size
export(float, 0.1, 25, 0.1) var factor = 5 setget set_factor, get_factor
export(int, 1, 500) var resolution = 32 setget set_resolution, get_resolution
export(int, 1, 200) var size = 50 setget set_size, get_size

func _init():
	update_heightmap()

func _update_mesh():
	heightmap = self.heightmap
	factor = self.factor
	resolution = self.resolution
	size = float(self.size)

	var origin = Vector3(-size/2, 0, -size/2)
	var res_size = size/resolution

	var image
	var w
	var h

	var get_height = false

	if heightmap:
		image = heightmap.get_data()
		image.lock()
		print("got image %s x %s" % [image.get_width(), image.get_height()])

		if image.get_width() > 0 and image.get_height() > 0:
			get_height = true

			w = image.get_width() - 1
			h = image.get_height() - 1
		else:
			get_height = false

	var tiles
	if tilemap:
		tiles = File.new()
		tiles.open(tilemap, File.READ)

	var surf = SurfaceTool.new()

	surf.begin(Mesh.PRIMITIVE_TRIANGLES)
	surf.add_smooth_group(true)

	var uv_size = 1.0 / float(tileset_size)

	for j in range(resolution):
		var tiles_line
		if tilemap:
			tiles_line = tiles.get_csv_line()
		for i in range(resolution):
			var vertex_height = [0, 0, 0, 0]

			if get_height:
				vertex_height[0] = image.get_pixel(w * float(i)/resolution, h * float(j)/resolution).gray() * factor
				vertex_height[1] = image.get_pixel(w * float(i+1)/resolution, h * float(j)/resolution).gray() * factor
				vertex_height[2] = image.get_pixel(w * float(i+1)/resolution, h * float(j+1)/resolution).gray() * factor
				vertex_height[3] = image.get_pixel(w * float(i)/resolution, h * float(j+1)/resolution).gray() * factor

			var tile = 0
			if tilemap:
				tile = int(tiles_line[i]) - 1
			var uv_x = float(tile % tileset_size) * uv_size
			var uv_y = float(tile / tileset_size) * uv_size

			# Make sure we split the quad along the shortest diagonal
			var vertices = [
				Vector3(i * res_size, vertex_height[0], j * res_size),
				Vector3((i+1) * res_size, vertex_height[1], j * res_size),
				Vector3((i+1) * res_size, vertex_height[2], (j+1) * res_size),
				Vector3(i * res_size, vertex_height[3], (j+1) * res_size),
			]
			if (vertices[2] - vertices[0]).length_squared() < (vertices[3] - vertices[1]).length_squared():
				surf.add_uv(Vector2(uv_x, uv_y))
				surf.add_vertex(vertices[0] + origin)
				surf.add_uv(Vector2(uv_x + uv_size, uv_y))
				surf.add_vertex(vertices[1] + origin)
				surf.add_uv(Vector2(uv_x + uv_size, uv_y + uv_size))
				surf.add_vertex(vertices[2] + origin)

				surf.add_uv(Vector2(uv_x, uv_y))
				surf.add_vertex(vertices[0] + origin)
				surf.add_uv(Vector2(uv_x + uv_size, uv_y + uv_size))
				surf.add_vertex(vertices[2] + origin)
				surf.add_uv(Vector2(uv_x, uv_y + uv_size))
				surf.add_vertex(vertices[3] + origin)
			else:
				surf.add_uv(Vector2(uv_x + uv_size, uv_y))
				surf.add_vertex(vertices[1] + origin)
				surf.add_uv(Vector2(uv_x + uv_size, uv_y + uv_size))
				surf.add_vertex(vertices[2] + origin)
				surf.add_uv(Vector2(uv_x, uv_y + uv_size))
				surf.add_vertex(vertices[3] + origin)

				surf.add_uv(Vector2(uv_x, uv_y + uv_size))
				surf.add_vertex(vertices[3] + origin)
				surf.add_uv(Vector2(uv_x, uv_y))
				surf.add_vertex(vertices[0] + origin)
				surf.add_uv(Vector2(uv_x + uv_size, uv_y))
				surf.add_vertex(vertices[1] + origin)

	if tilemap:
		tiles.close()

	surf.generate_normals()
	surf.index()

	var mesh = surf.commit()
	surf.clear()

	return mesh

func update_heightmap():
	var new_mesh = _update_mesh()

	new_mesh.set_name('Heightmap')
	set_mesh(new_mesh)

	if get_child_count():
		if get_child(0) is StaticBody:
			var sb = get_child(0)
			remove_child(sb)
			sb.queue_free()
			create_trimesh_collision()
	else:
		create_trimesh_collision()

#Setter functions
func set_heightmap(newvalue):
	heightmap = newvalue

	update_heightmap()

func set_tilemap(newvalue):
	tilemap = newvalue

	update_heightmap()

func set_tileset_size(newvalue):
	tileset_size = newvalue

	update_heightmap()

func set_factor(newvalue):
	factor = newvalue

	update_heightmap()

func set_resolution(newvalue):
	resolution = newvalue

	update_heightmap()

func set_size(newvalue):
	size = newvalue

	update_heightmap()

#Getter functions
func get_heightmap():
	return heightmap

func get_tilemap():
	return tilemap

func get_tileset_size():
	return tileset_size

func get_factor():
	return factor

func get_resolution():
	return resolution

func get_size():
	return size
