extends Node

const RETICLE_BLINK_TIME = 0.5

var character = null
var camera = null
onready var reticle = $"../../Reticle"

var reticle_anim_color = 0

func _ready():
	# Attach to character
	character = get_node("..")
	character.set_controller(self)

	# Set up a camera
	camera = $Camera
	remove_child(camera)
	character.get_node("CamBase").add_child(camera)

func _input(event):
	if event is InputEventKey and event.is_action("fire"):
		character.trigger(event.pressed)

func _process(delta):
	# Place reticle
	if character.target and character.target.get_ref():
		reticle.visible = true
		var pos = camera.unproject_position(character.target.get_ref().get_translation())
		reticle.set_position(pos - reticle.rect_size * reticle.rect_scale)
		if character.locked:
			reticle.set_rotation(reticle.get_rotation() + 6 * delta)
			reticle_anim_color = fmod(reticle_anim_color + delta, RETICLE_BLINK_TIME)
			var c = 1.0 - abs((reticle_anim_color - 0.5 * RETICLE_BLINK_TIME) / RETICLE_BLINK_TIME)
			reticle.modulate = Color(c, c, c)
		else:
			reticle.set_rotation(0)
			reticle.modulate = Color(1, 1, 1)
	else:
		reticle.visible = false

func _physics_process(delta):
	if not character.locked:
		var targets = get_tree().get_nodes_in_group("targets")
		var target = null
		var best_sqdist = 1000
		for t in targets:
			if t == self:
				continue

			var sqdist = (t.translation - character.translation).length_squared()
			if sqdist < best_sqdist:
				# Only acquire targets in front of us
				var relative = t.translation - character.translation
				relative = Vector2(relative.x, relative.z).normalized()
				var aim = Vector2(
				-character.buddy.transform.basis.z.x,
					-character.buddy.transform.basis.z.z
				)
				aim = aim.normalized()  # Needed because of scaling
				if relative.dot(aim) < 0.7:
					continue

				target = t
				best_sqdist = sqdist

		character.target = weakref(target) if target else null

	if Input.is_action_just_pressed("lock_target") and character.target:
		character.locked = true
	elif Input.is_action_just_released("lock_target"):
		character.locked = false

func get_movement():
	# Process input into direction vector
	var move_vec = Vector3()
	if Input.is_action_pressed("move_up"):
		move_vec.z -= 1
	if Input.is_action_pressed("move_down"):
		move_vec.z += 1
	if Input.is_action_pressed("move_right"):
		move_vec.x += 1
	if Input.is_action_pressed("move_left"):
		move_vec.x -= 1
	if Input.is_action_just_pressed("jump"):
		move_vec.y = 1
	return move_vec
